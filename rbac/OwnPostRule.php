<?php
namespace app\rbac;

use yii\rbac\Rule;
use app\models\User;
use app\models\Post;
use yii\web\NotFoundHttpException;
use Yii; 

class OwnPostRule extends Rule
{

	public $name = 'OwnPostRule';
	
	

	public function execute($user, $item, $params)
		{	
			$currentPost = Post::findOne($_GET['id']);
			
			if(isset($currentPost)){
				$author = $currentPost->created_by;
				
				if($author == $user){
					return true;
				}
			}
		
		
			return false;
		}
		
}