<?php

namespace app\models;

use yii\helpers\ArrayHelper;
use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use app\models\User;
use app\models\Category;
use app\models\Status;

/**
 * This is the model class for table "post".
 *
 * @property integer $id
 * @property string $title
 * @property string $body
 * @property string $category
 * @property string $author
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $created_by
 * @property string $updated_by
 */
class Post extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'safe'],
            [['title', 'body', 'category', 'author', 'status', 'created_by', 'updated_by'], 'string', 'max' => 255],
        ];
		
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'body' => 'Body',
            'category' => 'Category',
            'author' => 'Author',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
	public function beforeSave($insert) {
 
		if(parent::beforeSave($insert)) {
		
			if($this->hasAttribute("Id") && $insert && !Yii::$app->user->isGuest && $this->Id == null)
				$this->id = Yii::$app->user->id;
					
			if($this->hasAttribute("created_at") && $insert)
				$this->created_at = new Expression("now()");
		
			if($this->hasAttribute("created_by") && $insert && !Yii::$app->user->isGuest)
				$this->created_by = Yii::$app->user->id;
			
			if($this->hasAttribute("updated_at"))
				$this->updated_at = new Expression("now()");
		
			if($this->hasAttribute("updated_by") && !Yii::$app->user->isGuest)
				$this->updated_by = Yii::$app->user->id;
		
			return true;
		
			}
					
	}
	
	public function getCategoryItem()
    {
        return $this->hasOne(Category::className(), ['id' => 'category']);
    }	
	
	public function getStatusItem()
    {
        return $this->hasOne(Status::className(), ['id' => 'status']);
    }	
	public function getAuthorItem()
   {
       return $this->hasOne(User::className(), ['id' => 'author']);
   }
	
	
}
