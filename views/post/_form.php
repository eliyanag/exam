<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\User;
use app\models\Status;
use app\models\Category;


/* @var $this yii\web\View */
/* @var $model app\models\Post */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="post-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'body')->textarea(['rows' => 6]) ?>
	
	<?= $form->field($model, 'category')-> 
			dropDownList(/* שם טבלה*/ Category::getCategories()) ?> 
    <!--?= $form->field($model, 'category')->textInput(['maxlength' => true]) ?-->
	
	<?php if (\Yii::$app->user->can('deletePost')) { ?>
    <?= $form->field($model, 'author')->
			dropDownList(User::getUsers()) ?> 
	<?php } ?>
		
	<?php if (\Yii::$app->user->can('deletePost')) { ?>
			  <?= $form->field($model, 'status')->
			dropDownList(Status::getStatus()) ?>   
	<?php } ?>
   

    <!--?= $form->field($model, 'created_at')->textInput() ?-->

    <!--?= $form->field($model, 'updated_at')->textInput() ?-->

    <!--?= $form->field($model, 'created_by')->textInput(['maxlength' => true]) ?-->

    <!--?= $form->field($model, 'updated_by')->textInput(['maxlength' => true]) ?-->

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
